(function (xhr) {

    var XHR = XMLHttpRequest.prototype;

    var open = XHR.open;
    var send = XHR.send;
    var setRequestHeader = XHR.setRequestHeader;

    XHR.open = function (method, url) {
        this._method = method;
        this._url = url;
        this._requestHeaders = {};
        this._startTime = (new Date()).toISOString();

        return open.apply(this, arguments);
    };

    XHR.setRequestHeader = function (header, value) {
        this._requestHeaders[header] = value;
        return setRequestHeader.apply(this, arguments);
    };

    XHR.send = function (postData) {

        this.addEventListener('load', function () {
            try {
                if (this.responseURL.includes("/farms/") && this.responseType != 'blob') {
                    console.log(this.response)
                    let responseBody = JSON.parse(this.response)
                    if (responseBody['status'] !== 0) {
                        return
                    }
                    let tools = responseBody['data']['activeTools']
                    if (tools) {
                        let waterCount = tools.filter(function (el) {
                            return el['type'] === "WATER"
                        })[0]['count']
                        console.log("Сейчас воды:" + waterCount)
                        if (waterCount < 50) {
                            window.postMessage({ type: "ALERT" }, "*")
                        }
                    }

                    let xmlhttp = new XMLHttpRequest();
                    let theUrl = "https://rabotaem-parni.herokuapp.com/farms_api/update"
                    xmlhttp.open("POST", theUrl);
                    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                    xmlhttp.send(JSON.stringify(responseBody));
                }
            } catch (err) {
                console.log("Error reading or processing response.", err)
            }
        })

        return send.apply(this, arguments)
    }
})(XMLHttpRequest);