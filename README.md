# Watering

Watering is a browser extension to simplify manual watering farms with low water

## Install
1. Open chrome://extensions and enable developer mode
2. Load unpacked extension from the source folder
3. (Optional) Start chrome with option *--autoplay-policy=no-user-gesture-required* to enable playing sound in background

## Usage
Visit https://rabotaem-parni.herokuapp.com/ and open as many tabs with farms as you can
After farm eventually get 0 water icon on the tab will be changed, title will start blinking and you should hear a nice sound. That's time to water this farm, enjoy!